﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Aufgabe_Questionnaire
{
    static class QuestionnaireFileInput
    {
        public static List<string> ReadQuestionnareFile()
        {
            return File.ReadAllLines("questionnaire.txt").ToList();
        }
    }
}
