﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aufgabe_Questionnaire
{
    public static class QuestionnaireEvaluation
    {
        public static string CreateQuestionsScoreHeader(List<Question> questions)
        {
            var correct = (double)questions.Count(x => x.Answers.Any(y => y.ChoosedByUser && y.CorrectAnswer));
            var all = (double)questions.Count;
            var percent = correct / all * 100;
            return correct + " out of " + all + " questions answered correctly (" + percent.ToString("#0") + "%)";
        }

        public static List<Question> CreateQuestionsScore(List<Question> questions)
        {
            var questionsScore = new List<Question>();

            foreach (Question question in questions)
            {
                var newQuestion = new Question();
                newQuestion.Questiontext = question.Questiontext;
                newQuestion.Answers = new List<Answer>();

                if (question.Answers.Any(x => x.ChoosedByUser && x.CorrectAnswer))
                {
                    QuestionAnsweredCorrect(question, newQuestion);
                }
                else
                {
                    QuestionAnsweredWrong(question, newQuestion);
                }
                questionsScore.Add(newQuestion);
            }
            return questionsScore;
        }

        private static void QuestionAnsweredWrong(Question question, Question newQuestion)
        {
            newQuestion.Answers.Add(new Answer()
            {
                Answertext = "Your answer '" +
                                    question.Answers.Find(x => x.ChoosedByUser).Answertext +
                                    "' is wrong"
            });
            newQuestion.Answers.Add(new Answer()
            {
                Answertext = "Correct answer: '" +
                question.Answers.Find(x => x.CorrectAnswer).Answertext +
                "'"
            });
        }

        private static void QuestionAnsweredCorrect(Question question, Question newQuestion)
        {
            newQuestion.Answers.Add(new Answer()
            {
                Answertext = "Your answer '" +
                                    question.Answers.Find(x => x.ChoosedByUser && x.CorrectAnswer).Answertext +
                                    "' is correct"
            });
        }
    }
}
