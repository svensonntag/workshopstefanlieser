﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;

namespace Aufgabe_Questionnaire
{
    public class QuestionnaireViewModel : INotifyPropertyChanged
    {
        public List<Question> Questions { get; set; }

        private List<Question> _questionsScore;
        public List<Question> QuestionsScore {
            get {
                return _questionsScore;
            }
            set {
                _questionsScore = value;
                OnPropertyChanged();
            }
        }

        private Boolean _showQuestions;
        public Boolean ShowQuestions {
            get {
                return _showQuestions; }
            set {
                _showQuestions = value;
                OnPropertyChanged();
            }
        }

        private Boolean _showScore;
        public Boolean ShowScore {
            get {
                return _showScore;
            }
            set {
                _showScore = value;
                OnPropertyChanged();
            }
        }

        private string _questionScoreHeader;
        public string QuestionScoreHeader {
            get {
                return _questionScoreHeader;
            }
            set {
                _questionScoreHeader = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string name = "") {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private ICommand _showUserScore;
        public ICommand ShowUserScore {
            get {
                return _showUserScore ?? (_showUserScore = new ActionCommand(ShowUserScoreExecuted, ShowUserScoreCanExecute));
            }
        }

        public bool ShowUserScoreCanExecute(object param) {
            return Question.AllQuestionsAnswered(Questions);
        }

        public void ShowUserScoreExecuted(object param) {
            ShowQuestions = false;
            ShowScore = true;
            QuestionScoreHeader = QuestionnaireEvaluation.CreateQuestionsScoreHeader(Questions);
            QuestionsScore = QuestionnaireEvaluation.CreateQuestionsScore(Questions);
        }

        public QuestionnaireViewModel() :this(new List<string>())
        {
        }

        public QuestionnaireViewModel(List<string> fileAsList) {
            ShowQuestions = true;
            Questions = Question.CreateQuestionsList(fileAsList);
        }
    }
}
