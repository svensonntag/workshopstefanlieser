﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Aufgabe_Questionnaire
{
    public class Answer
    {
        private const char CorrectAnswerMarker = '*';
        public int QuestionNumber { get; set; }
        public string Answertext { get; set; }
        public bool CorrectAnswer { get; set; }
        public bool ChoosedByUser { get; set; }

        public static Answer CreateAnswer(string line, List<Question> questions)
        {
            return new Answer
            {
                QuestionNumber = GetQuestionNumber(questions),
                CorrectAnswer = line.StartsWith(CorrectAnswerMarker.ToString(), StringComparison.Ordinal),
                Answertext = line.TrimStart(CorrectAnswerMarker)
            };
        }

        public static Answer CreateDontKnowAnswer(List<Question> questions)
        {
            return new Answer
            {
                QuestionNumber = GetQuestionNumber(questions),
                CorrectAnswer = false,
                Answertext = "Don't know"
            };
        }

        private static int GetQuestionNumber(List<Question> questions)
        {
            return questions.IndexOf(questions.Last());
        }
    }
}
