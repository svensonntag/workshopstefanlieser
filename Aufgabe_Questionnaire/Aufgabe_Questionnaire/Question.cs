﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Aufgabe_Questionnaire
{
    public class Question
    {
        public string Questiontext { get; set; }
        public List<Answer> Answers { get; set; }

        public static bool AllQuestionsAnswered(List<Question> questions)
        {
            return questions.Count(f => f.Answers.Any(x => x.ChoosedByUser)) == questions.Count;
        }

        public static Question CreateQuestion(string line)
        {
            var newQuestion = new Question();
            newQuestion.Questiontext = line.Length > 1 ? line.TrimStart('?') + "?" : "";
            newQuestion.Answers = new List<Answer>();
            return newQuestion;
        }

        public static List<Question> CreateQuestionsList(List<string> fileAsList)
        {
            var questions = new List<Question>();

            foreach (string line in fileAsList)
            {
                if (LineContainsQuestion(line))
                {
                    var newQuestion = CreateQuestion(line);
                    questions.Add(newQuestion);
                    var dontKnowAnswer = Answer.CreateDontKnowAnswer(questions);
                    questions.Last().Answers.Add(dontKnowAnswer);
                }
                else
                {
                    var newAnswer = Answer.CreateAnswer(line, questions);
                    questions.Last().Answers.Insert(questions.Last().Answers.Count - 1, newAnswer);
                }
            }

            return questions;
        }

        private static bool LineContainsQuestion(string line)
        {
            return line.StartsWith("?", StringComparison.Ordinal);
        }
    }
}
