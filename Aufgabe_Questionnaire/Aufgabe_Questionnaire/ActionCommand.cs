﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Aufgabe_Questionnaire
{
    public class ActionCommand : ICommand
    {

        private readonly Action<object> _executeHandler;

        private readonly Func<object, bool> _canExecuteHandler;

        public ActionCommand(Action<object> execute, Func<object, bool> canExecute)
        {
            if (execute != null)
            {
                _executeHandler = execute;
                // Throw New ArgumentNullException("Execute cannot be null")
            }
            if (canExecute != null)
            {
                _canExecuteHandler = canExecute;
            }
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }

        }
        public void Execute(object parameter)
        {
            _executeHandler(parameter);
        }

        public bool CanExecute(object parameter)
        {
            if (_canExecuteHandler == null)
            {
                return true;
            }
            return _canExecuteHandler(parameter);
        }
    }
}