﻿using System.Windows;

namespace Aufgabe_Questionnaire
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            var lines = QuestionnaireFileInput.ReadQuestionnareFile();
            this.DataContext = new QuestionnaireViewModel(lines);
        }
    }
}
