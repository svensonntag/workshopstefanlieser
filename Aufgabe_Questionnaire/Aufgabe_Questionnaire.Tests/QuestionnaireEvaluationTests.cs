﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aufgabe_Questionnaire.Tests
{
    [TestFixture]
    public class QuestionnaireEvaluationTests
    {
        private List<Question> questionList;
        private List<Question> questionList2;

        [SetUp]
        public void QuestionTestsSetup()
        {
            questionList = Question.CreateQuestionsList(new List<string>() { "?a", "*b", "c", "d", "?e", "f", "*g", "h", "?i", "j", "*k" });
            questionList[0].Answers[0].ChoosedByUser = true;
            questionList[1].Answers[2].ChoosedByUser = true;
            questionList[2].Answers[2].ChoosedByUser = true;

            questionList2 = Question.CreateQuestionsList(new List<string>() { "?a", "*b", "c", "d", "?e", "f", "*g", "h", "?i", "j", "*k" });
            questionList2[0].Answers[0].ChoosedByUser = true;
            questionList2[1].Answers[2].ChoosedByUser = true;
        }

        [Test]
        public void CreateQuestionsScoreTest()
        {
            var result = QuestionnaireEvaluation.CreateQuestionsScore(questionList);

            Assert.That(result[0].Questiontext, Is.EqualTo("a?"));
            Assert.That(result[1].Questiontext, Is.EqualTo("e?"));
            Assert.That(result[2].Questiontext, Is.EqualTo("i?"));
            Assert.That(result[0].Answers[0].Answertext, Is.EqualTo("Your answer 'b' is correct"));
            Assert.That(result[1].Answers[0].Answertext, Is.EqualTo("Your answer 'h' is wrong"));
            Assert.That(result[1].Answers[1].Answertext, Is.EqualTo("Correct answer: 'g'"));
            Assert.That(result[2].Answers[0].Answertext, Is.EqualTo("Your answer 'Don't know' is wrong"));
            Assert.That(result[2].Answers[1].Answertext, Is.EqualTo("Correct answer: 'k'"));
        }

        [Test]
        public void CreateQuestionsScoreHeaderTest()
        {
            var result = QuestionnaireEvaluation.CreateQuestionsScoreHeader(questionList);

            Assert.That(result, Is.EqualTo("1 out of 3 questions answered correctly (33%)"));
        }

        [Test]
        public void AllQuestionsAnsweredTrueTest()
        {
            var result = Question.AllQuestionsAnswered(questionList);

            Assert.That(result, Is.EqualTo(true));
        }

        [Test]
        public void AllQuestionsAnsweredFalseTest()
        {
            var result = Question.AllQuestionsAnswered(questionList2);

            Assert.That(result, Is.EqualTo(false));
        }
    }
}
