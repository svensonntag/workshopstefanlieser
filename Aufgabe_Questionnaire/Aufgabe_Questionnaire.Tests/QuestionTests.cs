﻿using System;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace Aufgabe_Questionnaire.Tests
{
    [TestFixture]
    public class QuestionTests
    {

        [Test]
        public void CreateQuestionTest()
        {
            var result = Question.CreateQuestion("?a");

            Assert.That(result.Questiontext, Is.EqualTo("a?"));
            Assert.That(result.Answers.Count, Is.EqualTo(0));
        }

        [Test]
        public void CreateEmptyQuestionTest()
        {
            var result = Question.CreateQuestion("?");

            Assert.That(result.Questiontext, Is.EqualTo(""));
            Assert.That(result.Answers.Count, Is.EqualTo(0));
        }

        [Test]
        public void CreateQuestionsListOneTrueTest()
        {
            var result = Question.CreateQuestionsList(new List<string>() {"?a", "b", "c", "*d" });

            Assert.That(result.Count, Is.EqualTo(1));
            Assert.That(result.First().Questiontext, Is.EqualTo("a?"));
            Assert.That(result.First().Answers.Count, Is.EqualTo(4));
            Assert.That(result.First().Answers.First().Answertext, Is.EqualTo("b"));
            Assert.That(result.First().Answers.First().CorrectAnswer, Is.EqualTo(false));
            Assert.That(result.First().Answers.Where(x => x.CorrectAnswer).Count, Is.EqualTo(1));
            Assert.That(result.First().Answers.Where(x => !x.CorrectAnswer).Count, Is.EqualTo(3));
        }

        [Test]
        public void CreateQuestionsListTwoTrueTest()
        {
            var result = Question.CreateQuestionsList(new List<string>() { "?a", "*b", "c", "*d" });

            Assert.That(result.Count, Is.EqualTo(1));
            Assert.That(result.First().Questiontext, Is.EqualTo("a?"));
            Assert.That(result.First().Answers.Count, Is.EqualTo(4));
            Assert.That(result.First().Answers.First().Answertext, Is.EqualTo("b"));
            Assert.That(result.First().Answers.First().CorrectAnswer, Is.EqualTo(true));
            Assert.That(result.First().Answers.Where(x => x.CorrectAnswer).Count, Is.EqualTo(2));
            Assert.That(result.First().Answers.Where(x => !x.CorrectAnswer).Count, Is.EqualTo(2));
        }
    }
}
