﻿using System;
using NUnit.Framework;
using System.Collections.Generic;

namespace Aufgabe_Questionnaire.Tests
{
    [TestFixture]
    public class AnswerTests
    {
        private List<Question> questionList;
        private Question question;

        [SetUp]
        public void AnswerTestsSetup()
        {
            questionList = new List<Question>() { new Question() };
            question = new Question { Questiontext = "a?", Answers = new List<Answer>() {new Answer() } };
            questionList.Add(question);
        }

        [Test]
        public void CreateWrongAnswerTest()
        {
            var result = Answer.CreateAnswer("a", questionList);

            Assert.That(result.Answertext, Is.EqualTo("a"));
            Assert.That(result.CorrectAnswer, Is.EqualTo(false));
            Assert.That(result.QuestionNumber, Is.EqualTo(1));
            Assert.That(result.ChoosedByUser, Is.EqualTo(false));
        }

        [Test]
        public void CreateRightAnswerTest()
        {
            var result = Answer.CreateAnswer("*b", questionList);

            Assert.That(result.Answertext, Is.EqualTo("b"));
            Assert.That(result.CorrectAnswer, Is.EqualTo(true));
            Assert.That(result.QuestionNumber, Is.EqualTo(1));
            Assert.That(result.ChoosedByUser, Is.EqualTo(false));
        }

        [Test]
        public void CreateDontKnowAnswerTest()
        {
            var result = Answer.CreateDontKnowAnswer(questionList);

            Assert.That(result.Answertext, Is.EqualTo("Don't know"));
            Assert.That(result.CorrectAnswer, Is.EqualTo(false));
            Assert.That(result.QuestionNumber, Is.EqualTo(1));
            Assert.That(result.ChoosedByUser, Is.EqualTo(false));
        }
    }
}
